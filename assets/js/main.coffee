$ ->
  api_url = 'https://api.bycarrot.com/staff?include_alumni=1&include_dates=1&include_hidden=1&timestamp=1415823852&user_id=7&signature=%252FEz%252BgPa6wLGG%252F6PB0cCx7weA7ZXHFKpGdphB01bhUus%253D'

  $.get api_url, (res) ->
    # format the dates correctly
    res.data.shift()
    data = res.data.map (i) ->
      i.date_started = moment(i.date_started*1000)
      i.date_terminated = if i.date_terminated then moment(i.date_terminated*1000) else null
      return i

    # define and format data we need for the visualization
    start_of_company = moment('04/23/2005')
    today = moment()
    margin = { top: 20, right: 30, bottom: 30, left: 40 }
    width = 1150 - margin.left - margin.right
    height = 600 - margin.top - margin.bottom
    graph = d3.select('#graph')
      .attr('width', width + margin.left + margin.right)
      .attr('height', height + margin.top + margin.bottom)
      .append('g')
        .attr('transform', "translate(#{margin.left}, #{margin.top})")

    # loop by month and flatten out all the data
    all = []
    prev = []
    loop_by_month start_of_company, today, (month) ->
      employees = find_employed_staff(data, month)
      all.push
        date: new Date(JSON.parse(JSON.stringify(month)))
        employees: employees
        joined: _.difference(employees, prev)
        left: _.difference(prev, employees)
        departments: department_breakdown(employees)
      prev = employees

    # 
    # create the scales and axes for the data
    # 
    
    # y scale and axis
    y = d3.scale.linear()
      .domain([0, d3.max(all.map (i) -> i.employees.length)])
      .range([height, 0])

    y_axis = d3.svg.axis()
      .scale(y)
      .orient('left')

    # x scale and axis
    x = d3.scale.linear()
      .domain([0, all.length])
      .range([0, width])

    x_dates = d3.time.scale()
      .domain([all[0].date, all[all.length-1].date])
      .range([0, width])

    x_axis = d3.svg.axis()
      .scale(x_dates)
      .orient('bottom')

    # 
    # show the data on the graph
    # 

    # add the total employees containers on the graph
    bars = graph.selectAll('g').data(all).enter().append('g')
      .attr('transform', (d, i) -> "translate(#{x(1)*i},0)")

    bars.append('rect')
      .classed('bar', true)
      .attr('y', (d) -> y(d.employees.length))
      .attr('height', (d) -> height - y(d.employees.length))
      .attr('width', (d) -> x(1))

    # group for department sub-bars
    depts_group = bars.selectAll('g').data((d) -> [d]).enter().append('g')

    # department sub-bars
    depts_group.selectAll('rect').data((d) -> d.departments).enter().append('rect')
      .attr('class', (d) -> "dept #{d.name}")
      .attr('width', (d) -> x(1))
      .attr 'height', (d) ->
        parent = d3.select(this.parentNode.parentNode).select('rect')
        scale = d3.scale.linear()
          .domain([0, parent.datum().employees.length])
          .range([0, parent.attr('height')])
        scale(d.employees.length)
      .attr 'y', (d) ->
        bar = d3.select(this.parentNode.parentNode).select('rect')
        top_of_bar = bar.datum().employees.length
        total_height = bar.attr('height')
        this_height = d3.select(this).attr('height')

        prev = d3.select(this.previousSibling)
        
        if not prev[0][0]
          y(top_of_bar) + (parseInt(total_height) - parseInt(this_height))
        else
          parseInt(prev.attr('y')) - parseInt(d3.select(this).attr('height'))

    # employee add/loss interaction
    bars.on 'mouseover', (d) ->
      tpl = templates.join_leave(joined: d.joined, left: d.left)
      $('#info').html(tpl)

    # 
    # add the axes
    # 
    
    # y axis
    graph.append('g')
      .classed('y axis', true)
      .call(y_axis)
    .append("text")
      .attr("transform", "rotate(-90)")
      .attr("y", 6)
      .attr("dy", ".71em")
      .style("text-anchor", "end")
      .text("# of Employees")

    # x axis
    graph.append('g')
      .classed('x axis', true)
      .attr('transform', "translate(0, #{height})")
      .call(x_axis)

    # 
    # add the legend
    # 
    
    tpl = templates.legend(departments: get_departments(data), to_class: dept_to_class)
    $('#legend').html(tpl)
    

  ###*
   * @api private
  ###

  loop_by_month = (start, end, cb) ->
    while start < end
      cb(start)
      start.add(1, 'months')
  
  find_employed_staff = (all, month) ->
    start = month.startOf('month')
    end = month.endOf('month')

    all.filter (e) ->
      if not e.date_started.isBefore(start) then return false
      if not e.date_terminated then return true
      if e.date_terminated.isAfter(end) then return true else return false

  find_changed_staff = (all, month) ->
    start = moment(month).startOf('month')
    end = moment(month).endOf('month')

  department_breakdown = (employees) ->
    res = []
    for e in employees
      key = dept_to_class(e.department)
      dept = _.find(res, (i) -> i.name == key)
      if not dept then dept = { name: key, employees: [] }; res.push(dept)
      dept.employees.push(e)
    sort_by_name(res)

  sort_by_name = (arr) ->
    arr.sort (a, b) ->
      if a.name > b.name then return -1
      if a.name < b.name then return 1
      return 0

  dept_to_class = (name) ->
    _.str.underscored(name).replace('&', 'and')

  get_departments = (employees) ->
    _.uniq(_.collect(employees, 'department')).sort()
