axis         = require 'axis'
rupture      = require 'rupture'
autoprefixer = require 'autoprefixer-stylus'
js_pipeline  = require 'js-pipeline'
templates    = require 'client-templates'
s            = require 'underscore.string'

module.exports =
  ignores: ['readme.md', '**/layout.*', 'assets/**/_*', '.gitignore']

  stylus:
    use: [axis(), rupture(), autoprefixer()]

  extensions: [
    js_pipeline(manifest: 'assets/js/manifest.yml', minify: true),
    templates(base: 'assets/js/templates/', pattern: '*.jade', out: 'js/templates.js')
  ]

  locals: { s: s }

  server:
    clean_urls: true
